# ws@2023 fullAccess.tf

##################################################################
## FullAccess
##################################################################

data "aws_iam_policy" "iam_policy_fullAccess" {

  name = "AdministratorAccess"
}

# Ceate IAM Policy for STS: ${local.name}-STSFullAccessPolicy

resource "aws_iam_policy" "iam_policy_fullAccess_for_sts" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && local.fullAccess_enabled) ? 1 : 0

  name  = "${local.name}-STSFullAccessPolicy"

  description = "Policy for ${local.name}-STSFullAccess"

  policy = jsonencode(
    {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
    }
  )
}

#
resource "aws_iam_role" "iam_role_fullAccess_for_sts" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && local.fullAccess_enabled) ? 1 : 0

  name  = "${local.name}-STSFullAccessRole"

  max_session_duration = 28800 #8 hours

  assume_role_policy   = join("", data.aws_iam_policy_document.iam_policy_document_assume_role_for_sts.*.json)
  
  managed_policy_arns  = [ join("", aws_iam_policy.iam_policy_fullAccess_for_sts.*.arn) ]
  
  tags = merge(
    local.tags,
    {
      Name = "${local.name}-STSFullAccessRole"
    }
  )
}