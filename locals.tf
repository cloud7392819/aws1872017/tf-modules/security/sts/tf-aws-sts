# ws@2023 locals.tf

##################################################################
# Common/General settings
##################################################################

locals {

  # create
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false
    
  fullAccess_enabled  = var.fullAccess_enabled ? true : false
  testing_enabled  = var.testing_enabled ? true : false

  name = var.name  

  user_sts_name = "sts"
  user_sts_ssm_parametr_name = "/iam/users/sts/key"

  tags = merge(
    var.module_tags,
    {
      ENV = "STS"
    }
  )
}


