# ws@2023 data.tf

##################################################################
# Common/General settings
##################################################################

##################################################################
## STS
##################################################################

data "aws_iam_policy_document" "iam_policy_document_assume_role_for_sts" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0

  statement {

    actions = [
      "sts:AssumeRole",
      "sts:TagSession",
      "sts:SetSourceIdentity"
    ]
    
    principals {
      type = "AWS"
      identifiers = [aws_iam_user.default[count.index].arn]
    }

  }
}


