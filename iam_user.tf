# ws@2023 iam_user.tf

##################################################################
# Common/General settings
##################################################################

##################################################################
## Create IAM USER for STS
##################################################################

resource "aws_iam_user" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  

  name = local.user_sts_name

  path = "/"

  tags = merge(
    local.tags,
    {
      Name   = local.user_sts_name
    }
  )
}

##################################################################
## Create Key
##################################################################

resource "aws_iam_access_key" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  

  user = aws_iam_user.default[count.index].name

  lifecycle {
    ignore_changes = [
      status
    ]
  }

}

##################################################################
## Create SSM user parametr 
##################################################################

resource "aws_ssm_parameter" "default" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = local.create ? 1 : 0  

  name        = local.user_sts_ssm_parametr_name

  description = "Creds for User: ${local.user_sts_name}"

  type        = "SecureString"

  value = jsonencode(
    {
      USER_NAME              = aws_iam_access_key.default[count.index].user
      USER_ACCESS_KEY_ID     = aws_iam_access_key.default[count.index].id
      USER_SECRET_ACCESS_KEY = aws_iam_access_key.default[count.index].secret
    }
  )
}


