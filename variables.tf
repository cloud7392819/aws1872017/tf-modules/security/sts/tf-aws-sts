# ws@2023 variables.tf

##################################################################
# Common/General settings
##################################################################

variable "module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}

# Application name
variable "name" {
	description = "Application name"
	default		= null
}

variable "module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default     = ""
}

variable "module_tags" {
  description = "Additional mapping of tags to assign to the all linked resources."
  type        = map(string)
  default     = {}
}

##################################################################
# FullAccess settings
##################################################################

variable "fullAccess_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}


##################################################################
# Testing settings
##################################################################

variable "testing_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default     = false
}