# ws@2023 testing.tf

##################################################################
## TESTING
##################################################################

data "aws_iam_policy" "iam_policy_ec2_full_access" {

  name = "AmazonEC2FullAccess"
}


resource "aws_iam_role" "iam_role_testing" {

  # This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning: https://www.terraform.io/docs/provisioners/index.html
  count = (local.create && local.testing_enabled) ? 1 : 0

  name = "testing"

  #8 hours
  max_session_duration = 28800

  assume_role_policy  = join("", data.aws_iam_policy_document.iam_policy_document_assume_role_for_sts.*.json)
  
  managed_policy_arns = [ join("", data.aws_iam_policy.iam_policy_ec2_full_access.*.arn) ]
  
  tags = merge(
    local.tags,
    {
      Name = "testing"
    }
  )
}

